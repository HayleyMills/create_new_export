## create new export

loop over all prefix from Prefixes_to_export.txt
- click CREATE NEW EXPORT button
- download the newly created xml file
- check if any xml files contain missing question/statement literal
- clean text within xml
