#!/usr/bin/env python3

"""
Python 3
    Web scraping using selenium to click the Export button
"""

import pandas as pd
import time
import sys
import os

from mylib import get_driver, url_base, archivist_login_all, get_base_url

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException, NoSuchElementException

n = 5

def click_export_button(driver, df, uname, pw):
    """
    Loop over xml_name dictionary, click 'Export'
    """
    df_base_url = get_base_url(df)
    export_name = pd.Series(df_base_url.base_url.values, index=df.Instrument).to_dict()

    print("Got {} xml names".format(len(export_name)))
    # print(df)
    # print(export_name)

    ok = archivist_login_all(driver, export_name.values(), uname, pw)
    print(ok)

    k = 0
    pre_url = None
    msg_dict = {}
    for prefix, url in export_name.items():
        if url:
            print("Prefix number {}".format(k))

            if url != pre_url:
                print('Load : ' + url)
                driver.get(url)

                delay = 10*n # seconds
                try:
                    # find the input box
                    inputElement = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, "//input[@placeholder='Search by prefix (press return to perform search)']")))
                    print("Page is ready!")
                except TimeoutException:
                    print("Loading export page took too much time!")

            else:
                inputElement = pre_input
                # use backspace to clear text field
                inputElement.send_keys(Keys.CONTROL, 'a')
                inputElement.send_keys(Keys.BACKSPACE)

            print('Search prefix "{}"'.format(prefix))
            inputElement.send_keys(prefix)
            print("done")

            # choose from dropdown to display all results on one page
            select = Select(driver.find_element(By.XPATH, "//select[@aria-label='rows per page']"))

            # select by visible text
            select.select_by_visible_text('All')

            # locate id and link
            #TODO: make this better
            trs = driver.find_elements(by=By.XPATH, value="html/body/div/div/div/div/main/div/div/div/div/table/tbody/tr")

            print("This page has {} rows, searching for matching row".format(len(trs)))
            matching_idx = []
            for i in range(0, len(trs)):
                tr = trs[i]
                if prefix == tr.find_elements(By.XPATH, "td")[1].text:
                    matching_idx.append(i)
            if len(matching_idx) == 0:
                msg_dict[prefix] = "Could not find a row matching the prefix"
                continue
            elif len(matching_idx) > 1:
                msg_dict[prefix] = "There was more than one row matching this prefix: will download first"
                # note, keep going...?

            tr = trs[matching_idx[0]]

            # column 5 is "Actions", click on "Create new export"
            #TODO: this is case sensitive
            exportButton = tr.find_element(By.LINK_TEXT, "CREATE NEW EXPORT")
            # print(exportButton)
            print("Click export button for " + prefix)
            exportButton.click()
            time.sleep(n)

            pre_url = url
            pre_input = inputElement
        k = k + 1
    return msg_dict


def main():
    uname = sys.argv[1]
    pw = sys.argv[2]
    # prefixes
    df = pd.read_csv('Prefixes_to_export.txt', sep='\t')

    driver = get_driver()

    # click button
    message = click_export_button(driver, df, uname, pw)
    print(message)

    driver.quit()


if __name__ == "__main__":
    main()


